import Vue from 'vue'
import App from './App.vue'
import './assets/css/styles.scss';
import { BootstrapVue } from 'bootstrap-vue'
import VueI18n from 'vue-i18n'

Vue.use(BootstrapVue)
Vue.use(VueI18n)

Vue.config.productionTip = false;


const i18n = new VueI18n({
    locale: 'en',
    fallbackLocale: 'en',
    silentFallbackWarn: true,
    // it's required this is called messages.
    messages: {},
    sharedMessages: {}
});


new Vue({
  i18n,
  render: h => h(App),
}).$mount('#app')
